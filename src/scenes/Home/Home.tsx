import React from "react";
import Page from "../../components/Page/Page";

export const Home: React.FC = () => {
  return <Page title="Home">Hello, React XP!</Page>;
};
