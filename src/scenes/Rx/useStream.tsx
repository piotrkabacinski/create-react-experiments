import { useState, useEffect, useRef } from "react";
import { Observable, Subscription } from "rxjs";

export function useStream<T>(stream$: Observable<T>) {
  const [value, setValue] = useState<T>();
  const subscription = useRef<Subscription>();

  useEffect(() => {
    subscription.current = stream$.subscribe((value: any) => {
      setValue(value);
    });

    return () => {
      subscription.current && subscription.current.unsubscribe();
    };
  });

  return {
    value
  };
}
