import React from "react";
import Page from "../../components/Page/Page";
import { random } from "lodash";
import { Foo } from "./Foo/Foo";
import { Bar } from "./Bar/Bar";

import css from "./Rx.module.scss";
import classNames from "classnames";

export const items = [
  "👾",
  "🤖",
  "🍎",
  "⛅️",
  "🚀",
  "🎈",
  "😎",
  "🐷",
  "🦆",
  "🦄",
  "🐝",
  "🐻"
];
export const randomItem = () => items[random(0, items.length - 1)];

export const Wrapper: React.FC<{ border?: "green" | "blue" }> = ({
  children,
  border = ""
}) => {
  return (
    <div className={classNames(css.componentWrapper, css[border])}>
      {children}
    </div>
  );
};

export const Rx: React.FC = () => {
  return (
    <Page title="Rx">
      <Foo />
      <Bar />
    </Page>
  );
};
