import React, { useState, useEffect } from "react";
import { foo$ } from "../Foo/Foo";
import { Wrapper, randomItem } from "../Rx";

import { BehaviorSubject, Subscription } from "rxjs";
import { map } from "rxjs/operators";

export const bar$ = new BehaviorSubject<string>("");
let subscription$: Subscription;

export const Bar: React.FC = () => {
  const [message, setMessage] = useState<string>();

  useEffect(() => {
    subscription$ = foo$
      .pipe(map((value: string) => `Mapped: ${value}`))
      .subscribe((value: any) => {
        setMessage(value);
      });

    return () => {
      subscription$.unsubscribe();
    };
  });

  const handleClick = () => {
    bar$.next(`Hello from Bar! ${randomItem()}`);
  };

  return (
    <Wrapper>
      <h2>Bar</h2>
      <div>{message}</div>
      <button onClick={handleClick}>Behaviour</button>
    </Wrapper>
  );
};
