import React from "react";
import { Subject } from "rxjs";
import { Wrapper, randomItem } from "../Rx";
import { bar$ } from "../Bar/Bar";
import { useStream } from "../useStream";

export const foo$ = new Subject<string>();

export const Foo: React.FC = () => {
  const { value: message } = useStream<string>(bar$);

  const handleClick = () => {
    foo$.next(`Hello from Foo! ${randomItem()}`);
  };

  return (
    <Wrapper border="blue">
      <h2>Foo</h2>
      <div>{message}</div>
      <button onClick={handleClick}>Click!</button>
    </Wrapper>
  );
};
