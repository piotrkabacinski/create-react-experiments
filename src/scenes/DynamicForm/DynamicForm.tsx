import React from "react";
import Page from "../../components/Page/Page";
import { Formik, FieldArray, Form } from "formik";
import * as Yup from "yup";

import css from "./DynamicForm.module.scss";
import {
  TextField,
  SelectFieldOption,
  SelectField
} from "../../components/form";

const validationSchema = Yup.object().shape({
  genre: Yup.string().required(),
  actor: Yup.string().when("genre", {
    is: "3",
    then: Yup.string().required()
  }),
  movies: Yup.array()
    .of(
      Yup.object().shape({
        title: Yup.string().required("Title is required"),
        rating: Yup.number()
      })
    )
    .required()
});

type FormValues = Yup.InferType<typeof validationSchema>;

const intialValues: FormValues = {
  actor: "",
  genre: "",
  movies: [
    {
      title: "",
      rating: 1
    }
  ]
};

const genres: Array<SelectFieldOption> = [
  {
    value: 1,
    label: "Western"
  },
  {
    value: 2,
    label: "Horror"
  },
  {
    value: 3,
    label: "Sci-Fi"
  }
];

export const DynamicForm: React.FC = () => {
  const onSubmit = (values: FormValues) => {
    console.log(values);
  };

  return (
    <Page title="Dynamic Form">
      <Formik
        validationSchema={validationSchema}
        initialValues={intialValues}
        validateOnMount={true}
        onSubmit={onSubmit}
      >
        {({
          values: { movies, genre },
          values,
          errors,
          isValid,
          setFieldValue
        }) => {
          const sciFi = genres[2];

          const handleChange = () => {
            setFieldValue("actor", "");
          };

          return (
            <Form>
              <div className="mb-10">
                <SelectField
                  label="Genre"
                  name="genre"
                  options={genres}
                  isRequired
                  onChange={handleChange}
                />
              </div>

              {Number(genre) === sciFi.value && (
                <div>
                  <TextField
                    name="actor"
                    isRequired
                    label={`Favorite ${sciFi.label} actor`}
                  />
                </div>
              )}

              <FieldArray
                name="movies"
                render={arrayHelpers => {
                  const addMovie = () => {
                    arrayHelpers.push(intialValues.movies[0]);
                  };

                  const removeMovie = (index: number) => {
                    arrayHelpers.remove(index);
                  };

                  return (
                    <>
                      {movies.map((__, index) => (
                        <fieldset key={index} className={css.group}>
                          <div>
                            <TextField
                              name={`movies.${index}.title`}
                              label="Title"
                              isRequired
                            />

                            <TextField
                              type="number"
                              name={`movies.${index}.rating`}
                              label="Rating"
                              min="1"
                              max="10"
                              isRequired
                            />
                          </div>

                          <button
                            type="button"
                            onClick={() => removeMovie(index)}
                          >
                            Remove movie
                          </button>
                        </fieldset>
                      ))}
                      <button type="button" onClick={addMovie}>
                        Add movie
                      </button>
                    </>
                  );
                }}
              />
              <button type="submit" disabled={!isValid}>
                Submit
              </button>

              <div className="mt-10">
                Values: <code>{JSON.stringify(values)}</code>
              </div>

              <div>
                Errors: <code>{JSON.stringify(errors)}</code>
              </div>
            </Form>
          );
        }}
      </Formik>
    </Page>
  );
};
