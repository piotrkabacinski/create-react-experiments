import React, { useState } from "react";
import Page from "../../components/Page/Page";
import { Formik } from "formik";
import * as Yup from "yup";
import { TextField, VALIDATION_MESSAGES } from "../../components/form";
import { sleep } from "../../components/utils/utils";

const { required: requiredMessage } = VALIDATION_MESSAGES;

const validationSchema = Yup.object().shape({
  name: Yup.string()
    .min(3)
    .required(requiredMessage)
    .test("isNotJohn", "Can't be 'John'", async value => {
      await sleep(1000);
      return value !== "John";
    })
});

type FormValues = Yup.InferType<typeof validationSchema>;

const initialValues: FormValues = {
  name: ""
};

export const AsyncForm: React.FC = () => {
  const [isDisabled, setIsDiabled] = useState<boolean>(false);

  const onSubmit = (values: FormValues) => {
    console.log(values);
  };

  let timeout: any;

  return (
    <Page title="Async Form">
      <Formik
        initialValues={initialValues}
        validateOnMount={true}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
      >
        {({ handleSubmit, isValid, errors }) => {
          const nameOnChange = async (value: string) => {
            if (timeout) {
              clearTimeout(timeout);
            }

            if (errors["name"] !== undefined || value.length === 0) {
              return;
            }

            return await new Promise(resolve => {
              timeout = setTimeout(async () => {
                setIsDiabled(true);
                await sleep(3000);

                if (value === "John") {
                  resolve("Can't be 'John'!");
                } else {
                  resolve();
                }

                setIsDiabled(false);
              }, 500);
            });
          };

          return (
            <>
              <form onSubmit={handleSubmit}>
                <div>
                  {isDisabled && "Checking..."}
                  <TextField
                    name="name"
                    type="text"
                    label="Not 'John'"
                    validate={nameOnChange}
                    isRequired
                    isDisabled={isDisabled}
                  />
                </div>
                <button disabled={!isValid || isDisabled} type="submit">
                  Submit
                </button>
                <div className="mt-10">{JSON.stringify(errors)}</div>
              </form>
            </>
          );
        }}
      </Formik>
    </Page>
  );
};
