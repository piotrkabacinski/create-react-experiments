import React from "react";

export const NotFound: React.FC = () => {
  return (
    <div style={{ fontSize: "5rem" }}>
      <span role="img" aria-label="404">
        💩
      </span>
    </div>
  );
};
