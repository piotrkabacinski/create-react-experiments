import React from "react";
import Page from "../../components/Page/Page";
import { Formik } from "formik";
import * as Yup from "yup";
import {
  TextField,
  CheckboxField,
  VALIDATION_MESSAGES
} from "../../components/form";

const { required: requiredMessage } = VALIDATION_MESSAGES;

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .email("Invalid e-mail")
    .required(requiredMessage),
  password: Yup.string()
    .required(requiredMessage)
    .min(3, "Password is too short"),
  terms: Yup.bool()
    .oneOf([true], requiredMessage)
    .required()
});

type FormValues = Yup.InferType<typeof validationSchema>;

const initialValues: FormValues = {
  email: "",
  password: "",
  terms: false
};

export const BasicForm: React.FC = () => {
  const onSubmit = (values: FormValues) => {
    console.log(values);
  };

  const fillUp = (setValues: Function) => {
    setValues({
      email: "foo@example.com",
      password: "foobar",
      terms: true
    });
  };

  const reset = (setValues: Function) => {
    setValues(initialValues);
  };

  return (
    <Page title="Basic Form">
      <Formik
        initialValues={initialValues}
        validateOnMount={true}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
      >
        {({ handleSubmit, setValues, isValid }) => (
          <>
            <div className="mb-10">
              <button onClick={() => fillUp(setValues)}>Fill up form</button>
              <button onClick={() => reset(setValues)}>Reset form</button>
            </div>

            <form onSubmit={handleSubmit}>
              <div>
                <TextField
                  name="email"
                  type="email"
                  label="E-mail"
                  isRequired
                />
              </div>
              <div>
                <TextField
                  name="password"
                  type="password"
                  label="Password"
                  isRequired
                />
              </div>
              <div>
                <CheckboxField name="terms" isRequired>
                  Lorem ipsum
                </CheckboxField>
              </div>
              <button disabled={!isValid} type="submit">
                Submit
              </button>
            </form>
          </>
        )}
      </Formik>
    </Page>
  );
};
