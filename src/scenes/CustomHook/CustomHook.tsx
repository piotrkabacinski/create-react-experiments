import React, { useState } from "react";
import Page from "../../components/Page/Page";

const useDocumentTitle = (initialValue = "") => {
  const [title, setTitle] = useState(initialValue);
  const titleTag = document.querySelector("title");

  if (titleTag && initialValue) {
    titleTag.innerText = title;
  }

  return {
    title,
    setTitle
  };
};

export const CustomHook: React.FC = () => {
  const { title, setTitle } = useDocumentTitle("foo");
  const [isChanged, setIsChanged] = useState<boolean>(false);

  const handleClick = () => {
    setTitle("hello!");
    setIsChanged(true);
  };

  return (
    <Page title="Custom hook example">
      Document title: {title}
      <div>
        {!isChanged && (
          <button type="button" onClick={handleClick}>
            Update title
          </button>
        )}
      </div>
    </Page>
  );
};
