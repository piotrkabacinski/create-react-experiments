import React from "react";

interface PageProps {
  title?: string;
}

const Page: React.FC<PageProps> = ({ title, children }) => {
  return (
    <>
      {title ? <h1>{title}</h1> : null}
      {children}
    </>
  );
};

export default Page;
