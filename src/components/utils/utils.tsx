export const sleep = (timeout = 100) =>
  new Promise((resolve: Function) => setTimeout(() => resolve(), timeout));
