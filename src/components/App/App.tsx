import React, { ReactNode } from "react";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import {
  Home,
  CustomHook,
  BasicForm,
  NotFound,
  DynamicForm,
  AsyncForm,
  Rx
} from "../../scenes";

import Navigation from "../Navigation/Navigation";

import css from "./App.module.scss";

export interface PageRoute {
  to: string;
  name: string;
  component: ReactNode;
  exact?: boolean;
}

export const ROUTES: Array<PageRoute> = [
  {
    to: "/",
    name: "Home",
    exact: true,
    component: <Home />
  },
  {
    to: "/custom-hook",
    name: "Custom hook",
    component: <CustomHook />
  },
  {
    to: "/basic-form",
    name: "Basic form",
    component: <BasicForm />
  },
  {
    to: "/async-form",
    name: "Async form",
    component: <AsyncForm />
  },
  {
    to: "/dynamic-form",
    name: "Dynamic form",
    component: <DynamicForm />
  },
  {
    to: "/rx",
    name: "Rx",
    component: <Rx />
  }
];

const App: React.FC = () => {
  return (
    <Router>
      <div className={css.pageWrapper}>
        <header className={css.header}>
          <Navigation />
        </header>

        <main className={css.main}>
          <Switch>
            {ROUTES.map(({ to, exact = false, component }, index) => (
              <Route key={index} path={to} exact={exact}>
                {component}
              </Route>
            ))}
            <Route key="notFound">
              <NotFound />
            </Route>
          </Switch>
        </main>
      </div>
    </Router>
  );
};

export default App;
