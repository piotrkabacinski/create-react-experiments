import React from "react";
import { ErrorMessage, FormikErrors, FormikTouched, getIn } from "formik";

import css from "./FormError.module.scss";

export const VALIDATION_MESSAGES = {
  required: "Required field"
};

export function getErrorStyles(
  errors: FormikErrors<any>,
  touched: FormikTouched<any>,
  field: string,
  style: object
) {
  if (getIn(errors, field) && getIn(touched, field)) {
    return style;
  }
}

export const FormError: React.FC<{ name: string }> = ({ name }) => (
  <ErrorMessage component="div" className={css.formError} name={name} />
);
