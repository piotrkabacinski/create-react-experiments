import React from "react";
import { FormInputProps, FormError, FormLabel, getErrorStyles } from "..";
import { Field, FieldProps } from "formik";

export interface SelectFieldOption {
  value: string | number;
  label: string;
  onChange?: (value: SelectFieldOption["value"]) => void;
  onBlur?: (value: SelectFieldOption["value"]) => void;
}

interface SelectFieldProps extends Omit<FormInputProps, "type"> {
  options: Array<SelectFieldOption>;
  placeholder?: string;
}

export const SelectField: React.FC<SelectFieldProps> = ({
  name,
  label,
  isRequired,
  options,
  placeholder = "Select",
  onChange: onChangeCallback,
  onBlur: onBlurCallback
}) => (
  <>
    <div>
      <FormLabel label={label} fieldName={name} isRequired={isRequired} />

      <Field name={name}>
        {({
          field: { value, onChange, onBlur, ...field },
          form: { errors, touched }
        }: FieldProps) => (
          <select
            name={name}
            id={name}
            value={value}
            onChange={e => {
              onChange(e);

              if (onChangeCallback) {
                onChangeCallback(e.target.value);
              }
            }}
            onBlur={e => {
              onBlur(e);

              if (onBlurCallback) {
                onBlurCallback(e.target.value);
              }
            }}
            {...field}
            style={getErrorStyles(errors, touched, name, {
              border: "1px solid red"
            })}
          >
            <option disabled value="">
              {placeholder}
            </option>
            {options.map(({ value, label }) => (
              <option key={value} value={value}>
                {label}
              </option>
            ))}
          </select>
        )}
      </Field>
    </div>
    <FormError name={name} />
  </>
);
