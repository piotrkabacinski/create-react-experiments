import React from "react";
import { FormInputProps, FormError, FormLabel, getErrorStyles } from "..";
import css from "./CheckboxField.module.scss";
import { Field, FieldProps } from "formik";

export const CheckboxField: React.FC<Omit<
  FormInputProps,
  "label" | "type"
>> = ({ name, isRequired, children }) => (
  <>
    <div className={css.wrapper}>
      <Field name={name}>
        {({ field, form: { errors, touched } }: FieldProps) => (
          <>
            <div>
              <input
                type="checkbox"
                id={name}
                checked={field.value === true}
                {...field}
              />
            </div>
            <div>
              <FormLabel
                className={css.label}
                label={children}
                isRequired={isRequired}
                fieldName={name}
                style={getErrorStyles(errors, touched, name, {
                  color: "red"
                })}
              />
            </div>
          </>
        )}
      </Field>
    </div>
    <FormError name={name} />
  </>
);
