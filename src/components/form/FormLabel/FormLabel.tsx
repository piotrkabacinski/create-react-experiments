import React, { ReactNode } from "react";
import css from "./FormLabel.module.scss";

export const FormLabel: React.FC<{
  label: string | ReactNode;
  fieldName: string;
  isRequired?: boolean;
  className?: string;
  style?: object;
}> = ({ label, fieldName, isRequired = false, className, style }) => (
  <label className={className || css.label} htmlFor={fieldName} style={style}>
    {label} {isRequired ? "*" : ""}
  </label>
);
