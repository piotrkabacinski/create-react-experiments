import React from "react";
import { Field, FieldProps } from "formik";
import { FormError, getErrorStyles } from "../FormError/FormError";
import { FormLabel } from "..";

export interface FormInputProps {
  name: string;
  type?: string;
  label?: string;
  isTextArea?: boolean;
  isRequired?: boolean;
  isDisabled?: boolean;
  onChange?: (value: string | number) => void;
  onBlur?: (value: string | number) => void;
  [key: string]: any;
}

export const FormInput: React.FC<Omit<
  FormInputProps,
  "label" | "required"
>> = ({
  name,
  type,
  isTextArea = false,
  onChange: onChangeCallback,
  onBlur: onBlurCallback,
  isDisabled = false,
  ...props
}) => (
  <Field name={name} {...props}>
    {({
      field: { value, onChange, onBlur, ...field },
      form: { errors, touched }
    }: FieldProps) => (
      <input
        style={getErrorStyles(errors, touched, name, {
          border: "1px solid red"
        })}
        type={type}
        id={name}
        disabled={isDisabled}
        value={value}
        onChange={e => {
          onChange(e);

          if (onChangeCallback) {
            onChangeCallback(e.target.value);
          }
        }}
        onBlur={e => {
          onBlur(e);

          if (onBlurCallback) {
            onBlurCallback(e.target.value);
          }
        }}
        autoComplete="off"
        {...field}
      />
    )}
  </Field>
);

interface TextFieldProps extends Omit<FormInputProps, "type"> {
  type?: "text" | "number" | "password" | "email";
  [key: string]: any;
}

export const TextField: React.FC<TextFieldProps> = ({
  name,
  type = "text",
  label,
  isRequired = false,
  ...props
}) => (
  <>
    {label && (
      <FormLabel isRequired={isRequired} label={label} fieldName={name} />
    )}
    <FormInput name={name} type={type} {...props} />
    <FormError name={name} />
  </>
);
