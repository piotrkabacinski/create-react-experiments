import React from "react";
import { NavLink } from "react-router-dom";
import css from "./Navigation.module.scss";
import { PageRoute, ROUTES } from "../App/App";

const Link: React.FC<Omit<PageRoute, "component">> = ({ to, name, exact }) => (
  <li>
    <NavLink
      className={css.link}
      activeClassName={css.active}
      to={to}
      exact={exact}
    >
      {name}
    </NavLink>
  </li>
);

const Navigation: React.FC = () => {
  return (
    <nav>
      <ul className={css.ul}>
        {ROUTES.map(({ to, name, exact = false }, index) => (
          <Link key={index} to={to} exact={exact} name={name} />
        ))}
      </ul>
    </nav>
  );
};

export default Navigation;
